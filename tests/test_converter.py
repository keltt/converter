from converter import Converter

import unittest
import os


class ConverterTest(unittest.TestCase):
    def setUp(self):
        self.conv = Converter()


    def test_open_json(self):
        self.conv.open_json('test_data/test1.json')

        desired_data = [
            {
                'body': 'Hello, World 1!',
                'title': 'Title #1'
            },
            {
                'body': 'Hello, World 2!',
                'title': 'Title #2'
            }
        ]

        self.assertEqual(desired_data, self.conv.json_data, 'Incorrect data in open json')


    def test_convert_answer(self):
        desired_answer = "<p>title</p>"

        self.assertEqual(desired_answer, self.conv.create_content('p', 'title'), 'Incorrect answer')




