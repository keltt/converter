import json


class Converter:
    """
        Converts JSON to HTML.
        Output HTML in index.html file
    """

    def __init__(self, ):
        self.html_content = ""
        self.json_data = []

    def open_json(self, filename):
        self.html_content = ''
        with open(filename) as f:
            self.json_data = json.load(f)

    def save_html(self):
        with open('index.html', 'w') as f:
            f.write(self.html_content)

    # Create class, id selector from key value
    def handle_key(self, key):
        selectors = ''
        key_clear = key
        if '#' in key:
            key_split = key_clear.split('#')
            key_clear = key_split[0]
            selectors += ' id="%s"' % key_split[1]
        if '.' in key_clear:
            key_split = key_clear.split('.')
            key_clear = key_split[0]
            class_str = ""
            for i in range(1, len(key_split)):
                class_str += "%s " % key_split[i]
            selectors += ' class="%s"' % class_str[:-1]

        return key_clear, selectors

    # Make html as plain text
    def handle_value(self, value):
        clear_value = value.replace('<', '&lt;')
        clear_value = clear_value.replace('>', '&gt;')
        return clear_value

    def create_content(self, key, value, check_value=True):
        """

        :param key: name tag
        :param value: tag content
        :param check_value: Flag check value on html element. Default - True
        :return: string html text
        """
        key, selectors = self.handle_key(key)
        if check_value:
            value = self.handle_value(value)
        return "<{key}{selectors}>{val}</{key}>".format(key=key, selectors=selectors, val=value)

    # processing list
    def convert_list(self, json_list):
        html_temp = ""

        for item in json_list:
            if isinstance(item, list):
                html_part = self.convert_list(item)
            else:
                html_part = self.convert_dict(item)
            html_temp += "<li>%s</li>" % html_part

        return "<ul>%s</ul>" % html_temp

    # processing dict
    def convert_dict(self, json_dicts):
        html_temp = ''

        for k, v in json_dicts.items():
            if isinstance(v, list):
                v = self.convert_list(v)
                html_temp += self.create_content(k, v, check_value=False)
            else:
                html_temp += self.create_content(k, v)

        return html_temp

    def handler(self):
        if isinstance(self.json_data, list):
            self.html_content = self.convert_list(self.json_data)
        elif isinstance(self.json_data, dict):
            self.html_content = self.convert_dict(self.json_data)

    def convert(self, filename):
        self.open_json(filename)
        self.handler()
        self.save_html()
        return True


if __name__ == '__main__':
    c = Converter()
    c.convert('source.json')
